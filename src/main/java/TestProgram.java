import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestProgram {
    // Selenium WebDriver tool is used to automate web application testing to verify that it works as expected.
    // It supports many browsers such as Firefox, Chrome, IE, and Safari.
    WebDriver driver;
    @BeforeClass
    public void setup() {
        //setup the chromedriver using WebDriverManager
        WebDriverManager.chromedriver().setup();
        //Create driver object for Chrome
        driver = new ChromeDriver();
    }

    @Test
    public void start()
    {
        //Navigate to a URL
        driver.get("https:google.com");
        driver.manage().window().maximize();

        //quit the browser
        driver.quit();
    }
    }
